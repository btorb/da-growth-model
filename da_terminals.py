"""
Implementation of the algorithm proposed by Adrian Moore to illustrate
the compounding effect of slight changes in branching probabilities
on the overall morphology of da neurons.
"""

import sys
import numpy as np
import matplotlib.pyplot as plt
import btmorph

"""
Experimental data of the number of initial dendritic segments in
wilde-type
1=5%
2=10%
3= 70%
4=10%
5=5%
"""
WT_STEMS=[0.0,0.05,0.15,0.85,0.95,1.0]

"""
Experimental data of the number of initial dendritic segments (non-WT)
1=0%
2=5%
3= 40%
4=45%
5=10%
"""
E_STEMS=[0.0,0.0,0.05,0.45,0.9,1.0]

def one_run(seed=0,adjustment=1.0,WT=True,test=False):
    """
    Run the model once with the given parameters

    Parameters
    ----------
    seed : int
        Random seed
    adjustment : float
        The adjustment for wild-type and non-WT. For WT, set to 1.0
    WT : boolean
        True for wild-type, False otherwise
    """
    np.random.seed(seed)
    terminal_points = 0
    
    # step 1
    # sprout a number of stem based on experimental data
    if WT:
        rv = np.random.random()
        for p in range(1,len(WT_STEMS)):
            if WT_STEMS[p-1] <= rv <= WT_STEMS[p]:
                terminal_points = p        
    else:
        rv = np.random.random()
        for p in range(1,len(E_STEMS)):
            if E_STEMS[p-1] <= rv <= E_STEMS[p]:
                terminal_points = p        
    if test: print("terminal points after step {0}: {1}".format(1,terminal_points))

    # step 2 BASED ON PROBABILITIES
    # sample from a truncated (at the lower end) Normal distribution
    for i in range(terminal_points):
        to_add = 2*np.random.randn()+(4.2*adjustment)
        to_add=2 if to_add < 2 else to_add 
        terminal_points = int(terminal_points + to_add)
    if test: print("terminal points after step {0}: {1}".format(2,terminal_points))

    # step 3
    # sprout according to observed branching probability (1/4 for WT)
    for i in range(terminal_points):
        if(np.random.random() < 1.0/4.0*adjustment):
            terminal_points = terminal_points + 1
    if test: print("terminal points after step {0}: {1}".format(3,terminal_points))

    # step 4
    # sprout according to observed branching probability (1/10 for WT)
    for i in range(terminal_points):
        if(np.random.random() < 1.0/10.0*adjustment):
            terminal_points = terminal_points + 1
    if test: print("terminal points after step {0}: {1}".format(4,terminal_points))

    return terminal_points

def test(N=10,adjustment=1.15):
    """
    To reproduce S14 of the MS, run this function once.
    """
    terminal_points = []
    for i in range(N):
        terminal_points.append(one_run(i,WT=1))
    tp_m = np.mean(terminal_points)

    terminal_points_adj = []
    for i in range(N):
        terminal_points_adj.append(one_run(i,adjustment=adjustment,WT=0))    
    
    tp_a_m = np.mean(terminal_points_adj)

    plt.figure()
    
    plt.subplot(311)
    plt.title("WT={0}, EXP={1}".format(tp_m,tp_a_m))
    plt.hist(terminal_points,color='red',alpha=0.5,label="WT")
    plt.hist(terminal_points_adj,color='blue',alpha=0.5,label="expanded")
    plt.legend(loc=0)
    plt.ylabel("count")
    plt.subplot(312)
    plt.scatter(range(len(terminal_points)),terminal_points,color='red',alpha=0.5,label="WT")
    plt.scatter(range(len(terminal_points_adj)),terminal_points_adj,color='blue',alpha=0.5,label="expanded")
    plt.legend(loc=0)
    plt.ylabel("count")
    plt.subplot(313)
    plt.boxplot([terminal_points,terminal_points_adj])
    plt.ylabel("terminal points")
    plt.xticks([1,2],["WT","expanded"])
    plt.grid(1)

    from scipy.stats import ks_2samp
    k,p = ks_2samp(terminal_points,terminal_points_adj)
    print("KS: k={0}, p={1}".format(k,p))
    

if __name__=="__main__":
    test(N=1000,adjustment=1.15)
